import { createStore, applyMiddleware, compose } from "redux";

import { LoginReducer } from "./components/login/Redux/Reducer/loginReducer";

import {
  useSelector as useReduxSelector,
  TypedUseSelectorHook,
} from "react-redux";
import thunk from "redux-thunk";

const composeEnhancers =
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  LoginReducer,
  composeEnhancers(applyMiddleware(thunk))
);
export type RootState = ReturnType<typeof LoginReducer>;
export const useSelector: TypedUseSelectorHook<RootState> = useReduxSelector;

export default store;
