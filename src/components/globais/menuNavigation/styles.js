import { NavLink } from "react-router-dom";
import styled from "styled-components";

export const MenuItem = styled(NavLink)`
  &&& {
    background: rgb(255, 255, 240, 0.5);
    margin-bottom: 32px;
    justify-content: center;
    align-items: center;
    display: flex;
    height: 40px;
    border-radius: 5px;
    color: black;
    text-decoration: none;
    cursor: pointer;
  }
  &&&:focus {
    box-shadow: inset 0 0 2px #eeeeee;
    outline: none;
  }

  &:hover {
    filter: brightness(1.2);
  }
`;

export const RPGMasterTypo = styled("div")`
  height: 15%;
  font-size: 36px;
  font-family: cursive;
  display: flex;
  align-items: center;
  justify-content: center;
`;
