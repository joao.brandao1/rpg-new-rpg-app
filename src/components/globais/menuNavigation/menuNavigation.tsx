import { MenuItem, RPGMasterTypo } from "./styles";

const MenuNavigation = () => {
  const pages = [
    { name: "Página Inicial", route: "summary" },
    { name: "Campanhas", route: "campaigns" },
    { name: "Rolar Dados", route: "rollDices" },
    { name: "Futuras Histórias", route: "history" },
  ];

  return (
    <div style={{ width: "15%", background: "LightSteelBlue" }}>
      <RPGMasterTypo>
        RPG
        <br />
        MASTER
      </RPGMasterTypo>
      <div
        style={{
          padding: "16px 16px",
        }}
      >
        {pages.map((page) => {
          return <MenuItem to={`/${page.route}`}>{page.name}</MenuItem>;
        })}
      </div>
    </div>
  );
};

export default MenuNavigation;
