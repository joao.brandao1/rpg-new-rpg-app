import React from "react";
import { useLocation } from "react-router";

const TopNavigation = () => {
  const location = useLocation();

  return (
    <div
      style={{
        height: "15%",
        zIndex: 100,
        boxShadow: "0px 2px 4px #D3D3D3",
      }}
    >
      {location.pathname.replace("/", "")}
    </div>
  );
};
export default TopNavigation;
