import * as actionTypes from "../Types/loginTypes";

export const loginAuthenticate = () => {
  return async (dispatch) => {
    dispatch({
      type: actionTypes.LOGIN_AUTHENTICATE,
    });
  };
};
