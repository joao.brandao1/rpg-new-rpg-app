import * as actionTypes from "../Types/loginTypes";

export const initialState = {
  login: false,
};

export const LoginReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case actionTypes.LOGIN_AUTHENTICATE:
      return { ...state, login: true };

    default:
      return state;
  }
};
