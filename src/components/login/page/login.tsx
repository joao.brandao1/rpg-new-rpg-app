import React from "react";
import {
  Box,
  Paper,
  Typography,
  Grid,
  TextField,
  Button,
} from "@material-ui/core";
import { StyledContainer } from "./styled";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import * as actions from "../Redux/Actions/loginActions";

const Login = () => {
  const disptach = useDispatch();
  const history = useHistory();
  const handleLogin = () => {
    disptach(actions.loginAuthenticate());
    history.push("/summary");
  };

  return (
    <StyledContainer>
      <Grid
        item
        xl={5}
        sm={5}
        lg={5}
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Paper style={{ height: "80%", width: "70%" }}>
          <Box
            style={{
              display: "flex",
              height: "18%",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Typography variant="h6">Faça seu login</Typography>
          </Box>
          <Box style={{ height: "82%" }}>
            <Box
              style={{
                height: "50%",
                padding: "32px 32px 0px 32px",
              }}
            >
              <TextField
                style={{ marginBottom: "16px", width: "100%" }}
                label="email"
                variant="outlined"
              />
              <TextField
                label="senha"
                variant="outlined"
                style={{ width: "100%" }}
              />
              <Box>
                <Button
                  style={{
                    border: "1px solid black",
                    marginTop: "16px",
                    width: "100%",
                  }}
                  onClick={handleLogin}
                >
                  Logar
                </Button>
              </Box>
            </Box>
            <Box
              style={{
                height: "25%",
                display: "grid",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <Box>
                <Button>Esqueceu sua senha?</Button>
              </Box>
              <Box>
                <Button>Criar nova conta</Button>
              </Box>
            </Box>
          </Box>
        </Paper>
      </Grid>
      <Grid item xl={7} sm={7} lg={7}></Grid>
    </StyledContainer>
  );
};

export default Login;
