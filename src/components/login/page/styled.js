import styled from "styled-components";
import { Container } from "@material-ui/core";
import BackgroundLogin from "../../../Assets/BackgroundLogin.jpg";

export const StyledContainer = styled(Container)`
  &&& {
    height: 100vh;
    width: 100%;
    display: flex;
    background-image: url(${BackgroundLogin});
    background-size: cover;
    max-width: none;
  }
`;
