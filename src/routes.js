import React, { Suspense } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import CampaignsPage from "./components/campaigns/page/campaignsPage";
import MenuNavigation from "./components/globais/menuNavigation/menuNavigation";
import TopNavigation from "./components/globais/topNavigation/topNavigation";
import HistoryPage from "./components/history/page/historyPage";
import Login from "./components/login/page/login";
import RollDicesPage from "./components/rollDices/page/rollDicesPage";
import Summary from "./components/summary/page/summary";
import { useSelector } from "./Redux";

const Routes = () => {
  const login = useSelector((state) => state.login);

  return (
    <Router>
      <Suspense fallback={<div></div>}>
        {login ? (
          <div style={{ display: "flex", height: "100vh" }}>
            <MenuNavigation />
            <div style={{ width: "100%" }}>
              <TopNavigation />
              <Switch>
                <Route path="/summary" component={Summary} exact />
                <Route path="/campaigns" component={CampaignsPage} />
                <Route path="/rollDices" component={RollDicesPage} />
                <Route path="/history" component={HistoryPage} />
              </Switch>
            </div>
          </div>
        ) : (
          <>
            <Switch>
              <Redirect from="/" to="/login" exact />
              <Route path="/login" component={Login} exact />
            </Switch>
          </>
        )}
      </Suspense>
    </Router>
  );
};
export default Routes;
